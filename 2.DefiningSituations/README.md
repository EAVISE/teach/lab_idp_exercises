# Defining Situations: oefeningen
Hieronder volgen enkele oefeningen. De bedoeling is dat jullie deze maken in een editor naar keuze, en vervolgens doormailen naar mij (s.vandevelde@kuleuven.be).
Gelieve elke opdracht als een apart tekstbestand op te slaan, met als naam de titel van de subsectie.

In deze oefeningen gaan we dieper in op het leren beschrijven van een situatie.
Er zal jullie telkens gevraagd worden om een vocabularium met bijhorende structuur op te stellen.


## kaart.idp

We zouden graag een kaart van enkele Europese landen willen voorstellen (Belgie, Frankrijk, Nederland, Luxemburg en Duitsland).
Het is belangrijk dat we de volgende zaken kunnen beschrijven:

* Welke twee landen aan elkaar grenzen
* Hoeveel inwoners elk land heeft
* In welke landen Frans een officiele landstaal is

Stel voor bovenstaande opdracht een vocabularium op, met bijpassende structuur.


## latijnsvierkant.idp

In een Latijns vierkant is het de bedoeling om in een <i>n</i>x<i>n</i> raster op elke rij de getallen 1 t.e.m. <i>n</i> in te vullen, zonder dat hetzelfde getal twee keer voorkomt in dezelfde rij of in dezelfde kolom.
Denk nu zelf eens na: welke concepten zouden we zoal nodig hebben om dit uit te drukken?

Beschrijf deze concepten in een vocabularium, en geef er een bijpassende structuur bij.
Gebruik als voorbeeld dit 3x3 Latijns vierkant.

| 1 |   | 2 |
|---|---|---|
|   | 1 |   |
| 3 |   | 1 |
