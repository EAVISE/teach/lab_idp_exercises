# 3.6 Counting

Hieronder volgen enkele oefeningen. De bedoeling is dat jullie deze maken in [de online editor](https://interactive-consultant.idp-z3.be/IDE), en vervolgens doormailen naar mij (s.vandevelde@kuleuven.be).
Gelieve elke opdracht als een apart tekstbestand op te slaan, met als naam de titel van de subsectie.

In deze oefeningen gaan we dieper in op aggregaten.

## punten.idp

Startende van [punten.idp](https://interactive-consultant.idp-z3.be/IDE?G4ewxghgRgrgNhATgTwAQDVUG8BQr%2BoAuyADgKaoAi4ZAdoagFwC82l6AKgDSoBS6AUR4cA4gHkAvngLFyGCAGsmrLAEESJOAEtIhLWVWIwACx6G9AMx364ZAJL0eAgLZQyAE3cfKW2gHMyAGceEUQIEmMdYNQABUQQdxgwQgBhMkRCKQIiUgoAZUIYd30YmHo6ZWwABgA6GoBGKqyCaXwAsgtCRlQACmowOgYAKnkFAEpUAFoAPlQAIRAQOFbUEjLCOm6e9EUJmdQCopL1uhwpHEJjMhAUVA5GTFwznBxAwkQkwsR8h%2BwVtfKtEqag02l0%2BkMJimswALGYMlorGAbPZ6NDUAA2LgrbK4-AuNyeby%2BAKBdEAVhCYQiUQpPDiCU%2BaQy6IxEhqzxwJHiA0S31Qzggvh6E1w2W5ZEIxAA%2BtzfIQes4EmQ4NKyAAPEgQWjuHrcA5jMacoA), laat IDP een relate `geeft(Docent, Vak)` genereren zodat:
* JVE zeker niet `ProductCert` of `EmbeddedDinges` geeft
* TGO het vak `Graphics` geeft
* Elk vak exact 1 docent heeft
* Elke docent behalve DVT minstens 2 vakken heeft.

Voeg vervolgens toe dat een docent niet meer dan 10 studiepunten aan vakken mag doceren.
Tip: naast het `count` aggregaat zijn er ook een `sum`, `min` en `max` aggregaat.
Voor het sum aggregaat is de syntax als volgt:

```
sum(lambda t in T: if COND then X else Y)
```
met COND een formule en X en Y beiden numerieke waardes.

De syntax van min en max is analoog.
