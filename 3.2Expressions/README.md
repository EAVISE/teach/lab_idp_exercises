# 3.2 FO in FO(.): Expressions

Hieronder volgen enkele oefeningen. De bedoeling is dat jullie deze maken in [de online editor](https://interactive-consultant.idp-z3.be/IDE), en vervolgens doormailen naar mij (s.vandevelde@kuleuven.be).
Gelieve elke opdracht als een apart tekstbestand op te slaan, met als naam de titel van de subsectie.

In deze oefeningen gaan we dieper in op het schrijven van uitdrukkingen in FO(.).


## pasen.idp

Startende van [deze specificatie](https://interactive-consultant.idp-z3.be/IDE?G4ewxghgRgrgNhATgTwAQDVUG8BQr%2BoAuyADgKaoAiEA5ngQO5lkDWZAdgCaoBcqAFNRoBKVAFoAfKgBCIEHBwBfHDgDOhRDDCEYiCgGVeGbPXxDeAXmyoAshAhdaAGioBLdqs7PUAdRAdPb0oQLjJELxoXdERXACsIlwAtCEIwhNREkIjURQA6U1QmVg5uHissDJS070zHGhz85RxCAAsyEBRUABUjTFwlFRJEcDJOXQoAWwh3flFcAlQhskJiAH0h90J%2BCZBOMjhVsgAPEgdOfi6XfWFhJSA), druk uit dat Pasen altijd in een weekend valt. *Tip:* maak een constante `pasen` aan.

Probeer nu om uit te drukken dat Pasen nooit op een zaterdag valt. Indien dit niet lukt, pas de specificatie dan aan zodat het wel mogelijk is.

## familie.idp

Maak een IDP specificatie dat de volgende relaties modeleert.
Introduceer telkens zelf de nodige concepten. 

1. Anne, Jef, Jos, Maria en Eline zijn mensen.
2. Anne en Maria identificeren als vrouw, Jef en Jos als man, en Eline als X.
2. Iedereen spreekt Nederlands, maar enkel Maria kan ook een aardig woordje Frans. 
4. Jos en Maria zijn obees.


Schrijf vervolgens volgende formules:

1. Iedereen is ouder dan 18.
2. De schoenmaat van de vrouwen is kleiner dan 38.
3. De haarkleur van Jos is rood.
4. Anne en Eline zijn getrouwd.