# Exercises IDP lab 2024-2025

Deze repository bevat de extra oefeningen voor het 2024-2025 IDP labo.
Het idee is dat je na elk hoofdstuk van de [online tutorial](https://interactive-idp.gitlab.io/) de oefeningen in het bijhorende mapje oplost.
Nadat je deze hebt gemaakt stuur je deze door naar s.vandevelde@kuleuven.be.
