# 3.4 FO in FO(.): Typical uses of Quantifiers

Hieronder volgt een oefening.
De bedoeling is dat jullie deze maken, en vervolgens doormailen naar mij (s.vandevelde@kuleuven.be).
Gelieve de opdracht als een apart tekstbestand op te slaan, met als naam de titel van de subsectie.


## circuit_advanced.idp

Maak de [Logic Circuit Advanced](https://interactive-idp.gitlab.io/sections/Logic_Circuit_Advanced/) oefening. Vergeet niet om deze ook nog op te slaan!
