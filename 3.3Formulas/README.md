# 3.3 FO in FO(.): Formulas

Hieronder volgen enkele oefeningen.
De bedoeling is dat jullie deze maken, en vervolgens doormailen naar mij (s.vandevelde@kuleuven.be).
Gelieve elke opdracht als een apart tekstbestand op te slaan, met als naam de titel van de subsectie.

In deze oefeningen gaan we dieper in op het schrijven van formules.


## circuit.idp

Maak de [Logic Circuit](https://interactive-idp.gitlab.io/sections/Logic_Circuit/) oefening. Vergeet niet om deze ook nog op te slaan! (extra vraag, is er een oplossing waarbij P en R true zijn?)

## map_coloring.idp

Maak de [Map Coloring](https://interactive-idp.gitlab.io/sections/Map_Coloring/) oefening. Ook hier: vergeet het niet op te slaan!
