# 3.1 An Easy Example: oefeningen
Hieronder volgen enkele oefeningen. De bedoeling is dat jullie deze maken in [de online editor](https://interactive-consultant.idp-z3.be/IDE), en vervolgens doormailen naar mij (s.vandevelde@kuleuven.be).
Gelieve elke opdracht als een apart tekstbestand op te slaan, met als naam de titel van de subsectie.

In deze oefeningen gaan we dieper in op het schrijven van simpele wiskundige formules.


## letterpuzzle.idp

Modelleer onderstaande letterpuzzel in een IDP specificatie. Hier geldt als extra dat klinkers even getallen voorstellen, en medeklinkers oneven getallen.

`AI + BA = CDE`

## ageriddle.idp

Schrijf een IDP specificatie om het volgende raadsel op te lossen:

Jef has three children. The youngest child is the same age as the first digit of Jef's age. The middle child is the same age as the second digit in Jef's age. The eldest child's age is equal to the sum of the ages of the younger two children.
The total age of the family is 33. How old is Jef?
