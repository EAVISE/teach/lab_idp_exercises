# 3.5 Representing definitions

Hieronder volgen enkele oefeningen. De bedoeling is dat jullie deze maken in [de online editor](https://interactive-consultant.idp-z3.be/IDE), en vervolgens doormailen naar mij (s.vandevelde@kuleuven.be).
Gelieve elke opdracht als een apart tekstbestand op te slaan, met als naam de titel van de subsectie.

In deze oefeningen gaan we dieper in op definities in FO(.).


## geslaagd.idp

Startende van [geslaagd.idp](https://interactive-consultant.idp-z3.be/IDE?G4ewxghgRgrgNhATgTwAQDVUG8BQr%2BoAuyADgKYYQDWeBx5qAymCImbfmQB4QC2ZAOwD6bAM7xCECIVQAuSlVQBaAHxMWbDqgDmZUQgjaAJnIXK1AIRAg4OAL45HowohhhCMNk1OZc6anIAvNioAIKIhACWAGaRYJFkcGQAkgKEADSoAKK8UGRGRvkAIpECuqKZAAqIIEZuhADCZBGodgB0OMysFLLBWAAMbW0ATP3tONx8giJ6ElIyvSHhUbHxiSlpqIBJhKgAjMOZOXkFxaXl23sA7FU1de5NLTsAHOMOjoQAFmSsaAAqPrg3iQamB8p4KLwIKUABQASmwWmBZEIxCEwNKhGhvFqiSE3BIEAERmhv0yjFhsPsjiAA), druk de volgende regel uit:

> Je bent geslaagd voor een vak als en slechts als je minstens een 10 haalt op dit vak.

Druk dit eerst uit met een equivalentie, en daarna met een definitie.

Een interessante eigenschap van definities is dat je meerdere regels onder elkaar kan uitdrukken, dat samen het concept definiëren.
Zo kan je b.v. constructies schrijven als 

```
{
    !x1, .., xn A <- F1.
    !x1, .., xn A <- F2.
}
```
waarbij F1 en F2 samen zowel de *necessary* als de *sufficient* conditions vormen.
Voeg nu een regel toe dat zegt dat je ook kan slagen met 8-9 als je het vak tolereert.
Probeer dit zowel bij de equivalentie als de definitie. Welke van de twee werkt beter?

## recursie.idp

Definities in FO(.) kunnen ook recursief zijn: m.a.w., ze kunnen een concept definiëren in termen van dat concept zelf.
In [recursie.idp](https://interactive-consultant.idp-z3.be/IDE?G4ewxghgRgrgNhATgTwAQDVUG8BQr%2BoAuyADgKaoByIAJhQFwC82AggDSoBCHAwhwCIcAogF88BMjQDmZelVoUAVPLqoAtAD4uIEHHH5EZCGAAW0OLJVKr6rZx16xOHAGdCiGGEIxDqAMqocpi4BKiSMoHMWAAU7FwAlByxvImo0dyoQon6oblpfKj8qdEFWUmCmdl51dFCHJzxIgB0OE44hCZkICioACqBGLitziSI4JI%2BFAC2EACWAHbR8dg5o2SExAD6owuE0VMKcJtkAB4kEPM00b0cfvHxwzhAA) is er een grafe voorgesteld met nodes A t.e.m. E.
Schrijf nu een definitie dat de *reachability* van nodes uitdrukt, d.w.z., welke nodes een pad hebben naar welke andere nodes.
B.v., E is reachable vanuit A, want A -> C -> E.

Er zijn dus twee regels dat je moet uiten:
* N2 is reachable vanuit N1 als er een edge is tussen de twee.
* N2 is reachable vanuit N1 als er een derde node is dat een pad heeft naar N1 en reachable is vanuit N2.
